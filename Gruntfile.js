module.exports = function (grunt) {
  require('time-grunt')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    paths: {
      dist: 'dist',
      src: 'src'
    },

    svg_sprite: {
      puIcons: {
        src: ["<%= paths.src %>/**/*.svg"],
        dest: "<%= paths.dist %>/sprite/",
        options: {
          render: {
            html: true,
            css: false,
            less: {
              dest: 'less/_sprite'
            }
          },
          maxwidth: 50,
          maxheight: 50,
          padding: 10,
          keep: true,
          dims: true
        }
      }
    },

    grunticon: {
      puIcons: {
        files: [{
          expand: true,
          cwd: '<%= paths.src %>',
          src: '*.svg',
          dest: '<%= paths.dist %>/grunticons/'
        }],
        options: {
          defaultWidth: "40px",
          defaultHeight: "40px"
        }
      }
    },

    webfont: {
      puIcons: {
        src: '<%= paths.src %>/*.svg',
        dest: '<%= paths.dist %>/webfonts',
        syntax: 'pu-icon',
        options: {
         font: 'pu-icons',
         fontFilename: 'pu-icons-{hash}',
         stylesheet: 'less',
         relativeFontPath: '/assets/fonts',
          templateOptions: {
            baseClass: 'pu-icon',
            classPrefix: 'pu-icon-',
            mixinPrefix: 'pu-icon_'
          }
        }
      }
    },
    less: {
      dist: {
        options: {
          paths: ['<%= paths.dist %>/webfonts']
        },
        // files: {
        //   'pu-icons.css': 'pu-icons.less'
        // },
        files: [{
          expand: true,
          cwd: '<%= paths.dist %>/webfonts',
          src: '*.less',
          dest: '<%= paths.dist %>/webfonts',
          ext: '.css'
        }]
      }
    }
  });

  require('load-grunt-tasks')(grunt);

  grunt.registerTask('default', ['grunticon:puIcons', 'webfont:puIcons', 'less:dist']);
  grunt.registerTask('iconfont', ['webfont', 'less:dist']);
};
